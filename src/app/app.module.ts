import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MyHttpInterceptor } from './my-http.interceptor';
// store
import { StoreModule } from '@ngrx/store';
import { userCacheReducer } from '@/state/user.reducer';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { zh_CN } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// markdown editor
import { EditorMdDirective } from './editor/editor-md.directive';

import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button'
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzTreeViewModule } from 'ng-zorro-antd/tree-view';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';

import { LayoutComponent } from './pages/layout/layout.component';
import { FeProjectsComponent } from './pages/fe-projects/fe-projects.component';

import icons from './icons';
import { HomeComponent } from './pages/home/home.component';
import { SiteIndexComponent } from './pages/site/index/index.component';
import { PCIndexComponent } from './pages/pc/index/index.component';
import { ProjectIndexComponent } from './pages/project/index/index.component';
import { IssuesComponent } from './pages/gitee/issues/issues.component';
import { ReposComponent } from './pages/gitee/repos/repos.component';
import { EnterprisesComponent } from './pages/gitee/enterprises/enterprises.component';
import { IssuesAddComponent } from './pages/gitee/issues-add/issues-add.component';
import { LoginComponent } from './pages/login/login.component';
import { Json2structComponent } from './pages/tools/json2struct/json2struct.component';
import { MenulistComponent } from './pages/menu/menulist/menulist.component';
import { MenuformComponent } from './pages/menu/menuform/menuform.component';
import { DialogInputComponent } from './pages/comps/dialog-input/dialog-input.component';
import { MenuitemComponent } from './pages/menu/menuitem/menuitem.component';
import { Text2qrcodeComponent } from './pages/tools/text2qrcode/text2qrcode.component';
import { EditorMdDemoComponent } from './pages/editor-md-demo/editor-md-demo.component';
import { DocsListComponent } from './pages/docs/docs-list/docs-list.component';
import { DocsPreviewComponent } from './pages/docs/docs-preview/docs-preview.component'

registerLocaleData(zh);


@NgModule({
  declarations: [
    AppComponent,
    EditorMdDirective,  //  editor.md https://blog.csdn.net/DoubleRam/article/details/118568165
    LayoutComponent,
    HomeComponent,
    FeProjectsComponent,
    SiteIndexComponent,
    PCIndexComponent,
    ProjectIndexComponent,
    IssuesComponent,
    ReposComponent,
    EnterprisesComponent,
    IssuesAddComponent,
    LoginComponent,
    Json2structComponent,
    MenulistComponent,
    MenuformComponent,
    DialogInputComponent,
    MenuitemComponent,
    Text2qrcodeComponent,
    EditorMdDemoComponent,
    DocsListComponent,
    DocsPreviewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot({ usercache3: userCacheReducer }),
    NzBadgeModule,
    NzIconModule,
    NzGridModule,
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzSelectModule,
    NzInputModule,
    NzAlertModule,
    NzCardModule,
    NzTabsModule,
    NzTableModule,
    NzDropDownModule,
    NzNotificationModule,
    NzModalModule,
    NzDividerModule,
    NzBreadCrumbModule,
    NzFormModule,
    NzCheckboxModule,
    NzRadioModule,
    NzMessageModule,
    NzTreeSelectModule,
    NzTreeViewModule,
    NzImageModule,
    NzListModule,
    NzSwitchModule,
    NzPaginationModule,
    NzPageHeaderModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
    // { provide: RouteReuseStrategy, useClass: RouteStrategyService }, // 使路由重用
    { provide: NZ_I18N, useValue: zh_CN },
    { provide: NZ_ICONS, useValue: icons }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
