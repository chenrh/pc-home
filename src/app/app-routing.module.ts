import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@/pages/home/home.component';
import { EditorMdDemoComponent } from './pages/editor-md-demo/editor-md-demo.component';
import { LayoutComponent } from '@/pages/layout/layout.component';
import { FeProjectsComponent } from '@/pages/fe-projects/fe-projects.component';
import { PCIndexComponent } from './pages/pc/index/index.component';
import { SiteIndexComponent } from './pages/site/index/index.component';
import { ReposComponent } from './pages/gitee/repos/repos.component'
import { IssuesComponent } from './pages/gitee/issues/issues.component'
import { EnterprisesComponent } from './pages/gitee/enterprises/enterprises.component'
import { IssuesAddComponent } from './pages/gitee/issues-add/issues-add.component';
import { LoginComponent } from './pages/login/login.component';
import { Json2structComponent } from './pages/tools/json2struct/json2struct.component';
import { Text2qrcodeComponent } from './pages/tools/text2qrcode/text2qrcode.component';
import { MenulistComponent } from './pages/menu/menulist/menulist.component';
import { MenuformComponent } from './pages/menu/menuform/menuform.component';
import { DocsListComponent } from './pages/docs/docs-list/docs-list.component';
import { DocsPreviewComponent } from './pages/docs/docs-preview/docs-preview.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/layout/home' },
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  {
    path: 'layout', component: LayoutComponent, children: [
      { path: 'home', component: HomeComponent },
      { path: 'editor', component: EditorMdDemoComponent },
      { path: 'fe-projects', component: FeProjectsComponent },
      { path: 'gitee/repos', component: ReposComponent },
      { path: 'gitee/issues/:owner/:repo', component: IssuesComponent },
      { path: 'gitee/issues/new', component: IssuesAddComponent },
      { path: 'gitee/enterprises', component: EnterprisesComponent },
      { path: 'pc/:id', component: PCIndexComponent },
      { path: 'site/:id', component: SiteIndexComponent },
      { path: 'json2struct', component: Json2structComponent },
      { path: 'text2qrcode', component: Text2qrcodeComponent },
      { path: 'menu', component: MenulistComponent },
      { path: 'menu/form', component: MenuformComponent },
      { path: 'docs/list', component: DocsListComponent },
      { path: 'docs/:id', component: DocsPreviewComponent },
      { path: 'editor/:id', component: EditorMdDemoComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
