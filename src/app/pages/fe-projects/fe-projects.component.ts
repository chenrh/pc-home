import { NodeModel } from '@/models/NodeModel';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fe-projects',
  templateUrl: './fe-projects.component.html',
  styleUrls: ['./fe-projects.component.less']
})
export class FeProjectsComponent implements OnInit {

  arrProject: Array<NodeModel> = []
  constructor() { }

  ngOnInit(): void {

    const mcsys: NodeModel = {
      id: "mc-sys",
      title: "军队物资管理系统",
      url: "http://192.168.1.41:8888/web-frontend/mc-sys",
    }
    this.arrProject.push(mcsys)

    const hvbox: NodeModel = {
      id: "hvbox",
      title: "高值柜系统",
      url: "http://192.168.1.41:8888/web-frontend/huatuo-tbox-ui",
    }
    this.arrProject.push(hvbox)

    const h5oa: NodeModel = {
      id: 'h5oa',
      title: 'h5 oa',
      url: 'http://192.168.1.81:8080/index.html?login_url=192.168.1.76&user_code=admin&router_to=work#/work?VNK=a1affb47'
    }
    this.arrProject.push(h5oa)
  }

}
