import { ApiPagination } from '@/models/ApiPagination';
import { DocService } from '@/service/doc.service';
import { Component, OnInit } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
  selector: 'app-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.less']
})
export class DocsListComponent implements OnInit {
  dataSet: any = []
  page: ApiPagination = { page: 1, per_page: 10, total: 0 }


  constructor(private docService: DocService) { }

  ngOnInit(): void {
  }
  loadData() {
    this.docService.getDocs(this.page).subscribe(el => {
      this.dataSet = el.list;
      this.page.total = el.total;
    })
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    // console.log(params)
    const { pageSize, pageIndex, sort, filter } = params;
    this.page.page = pageIndex;
    this.page.per_page = pageSize;

    // const currentSort = sort.find(item => item.value !== null);
    // const sortField = (currentSort && currentSort.key) || "full_name";
    // const sortOrder = (currentSort && currentSort.value) || "asc";

    // this.page.sort = sortField;
    // this.page.direction = sortOrder;

    this.loadData()
  }
}
