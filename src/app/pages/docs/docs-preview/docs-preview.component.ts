import { DocService } from '@/service/doc.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


declare var editormd: any;
declare var $: any;

@Component({
  selector: 'app-docs-preview',
  templateUrl: './docs-preview.component.html',
  styleUrls: ['./docs-preview.component.less']
})
export class DocsPreviewComponent implements OnInit {

  docID: string
  docName: string
  showMenu: boolean = false;


  constructor(private route: ActivatedRoute, private docService: DocService) {
    this.docID = route.snapshot.paramMap.get('id') || "";
    this.docName = "";
  }

  ngOnInit(): void {
    this.docService.getDocByID(this.docID).subscribe(el => {
      this.loadMD(el.data.doc_path);
      this.docName = el.data.doc_name;
    })
  }

  loadMD(doc_path: string) {
    $.get(doc_path, function (markdown: any) {
      var testEditormdView = editormd.markdownToHTML("doc-editormd-view", {
        markdown: markdown,//+ "\r\n" + $("#append-test").text(),
        //htmlDecode      : true,       // 开启 HTML 标签解析，为了安全性，默认不开启
        htmlDecode: "style,script,iframe",  // you can filter tags decode
        //toc             : false,
        tocm: true,    // Using [TOCM]
        tocContainer: "#custom-toc-container", // 自定义 ToC 容器层
        //gfm             : false,
        //tocDropdown     : true,
        // markdownSourceCode : true, // 是否保留 Markdown 源码，即是否删除保存源码的 Textarea 标签
        emoji: true,
        taskList: true,
        tex: true,  // 默认不解析
        flowChart: true,  // 默认不解析
        sequenceDiagram: true,  // 默认不解析
      });
    });
  }

}
