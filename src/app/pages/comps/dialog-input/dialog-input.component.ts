import { Component, Input } from '@angular/core';
// import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
@Component({
  selector: 'app-dialog-input',
  templateUrl: './dialog-input.component.html',
  styleUrls: ['./dialog-input.component.less']
})
export class DialogInputComponent {
  @Input() subtitle?: string;
  @Input() menu_name?: string;
  @Input() menu_icon?: string;
  @Input() order_num!: number;

  constructor() { }
  // constructor(private modal: NzModalRef) { }

  // destroyModal(): void {
  //   this.modal.destroy({ data: 'this the result data' });
  // }
}

/*
how to use:
addRootMenu(): void {
    const modal: NzModalRef = this.modal.create({
      nzTitle: '新增菜单',
      nzContent: DialogInputComponent,
      nzViewContainerRef: this.viewContainerRef,
      nzComponentParams: {
        subtitle: '根目录下的一级菜单名',
        placeholder: '请输入一级菜单名',
      },
      // nzOnOk: () => new Promise(resolve => setTimeout(resolve, 1000)),
      nzFooter: [
        {
          label: '关闭',
          onClick: () => modal.destroy()
        },
        {
          label: '保存',
          type: 'primary',
          onClick: componentInstance => {
            componentInstance!.subtitle = componentInstance?.inputValue;
          }
        }
      ]
    });
*/