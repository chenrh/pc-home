import { Component, OnInit } from '@angular/core';
import { RepoService } from '@/service/repo.service'
import { ApiPagination } from '@/models/ApiPagination';
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: ['./repos.component.less']
})
export class ReposComponent implements OnInit {

  dataSet: any = []
  page: ApiPagination = { page: 1, per_page: 10, total: 0 }

  constructor(private repoService: RepoService) { }

  ngOnInit(): void {
    // 前端控件 会自己调 onQueryParamsChange
  }

  loadData() {
    this.repoService.getRepos(this.page).subscribe(el => {
      this.dataSet = el.list;
      this.page.total = el.total;
    })
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    // console.log(params)
    const { pageSize, pageIndex, sort, filter } = params;
    this.page.page = pageIndex;
    this.page.per_page = pageSize;

    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || "full_name";
    const sortOrder = (currentSort && currentSort.value) || "asc";

    this.page.sort = sortField;
    this.page.direction = sortOrder;

    this.loadData()

    // this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }
}
