import { ApiIssuesPagination } from '@/models/ApiPagination';
import { RepoService } from '@/service/repo.service';
import { Component, OnInit } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { Router } from '@angular/router';
@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.less']
})
export class EnterprisesComponent implements OnInit {
  dataSet: any = []
  page: ApiIssuesPagination = { page: 1, per_page: 10, total: 0, state: 'open' }

  constructor(private router: Router, private repoService: RepoService) { }

  ngOnInit(): void {
  }

  loadData() {
    this.repoService.getEnterprisesIssues(this.page).subscribe(el => {
      this.dataSet = el.list;
      this.page.total = el.total;
    })
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    // console.log(params)
    const { pageSize, pageIndex, sort, filter } = params;
    this.page.page = pageIndex;
    this.page.per_page = pageSize;

    const currentSort = sort.find(item => item.value !== null);
    if (currentSort) {
      this.page.sort = (currentSort && currentSort.key);
      this.page.direction = (currentSort && currentSort.value) || "desc";
    }
    this.loadData()
  }

  newIssue(): void {
    this.router.navigateByUrl("layout/gitee/issues/new");
  }
}
