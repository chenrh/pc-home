import { ApiIssuesPagination } from '@/models/ApiPagination';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RepoService } from '@/service/repo.service'
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.less']
})
export class IssuesComponent implements OnInit {

  dataSet: any = []
  page: ApiIssuesPagination

  constructor(private route: ActivatedRoute, private router: Router, private issuseService: RepoService) {
    const owner = this.route.snapshot.paramMap.get('owner') || "chenrh";
    const repo = this.route.snapshot.paramMap.get('repo') || "gbox";
    this.page = { page: 1, per_page: 10, total: 0, owner, repo, state: "open" }
  }

  ngOnInit(): void {

  }

  loadData() {
    this.issuseService.getIssues(this.page).subscribe(el => {
      this.dataSet = el;
    })
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    // console.log(params)
    const { pageSize, pageIndex, sort, filter } = params;
    this.page.page = pageIndex;
    this.page.per_page = pageSize;

    this.loadData()
  }


  newIssue(): void {
    this.router.navigateByUrl(`layout/gitee/issues/new?repo=${this.page.repo}&owner=${this.page.owner}`);
  }

}
