import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserCache } from '@/state/UserCache';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  userCache?: UserCache;

  data = [
    { h: 'https://e.gitee.com/cheerz/docs?directory=996&page=1&parent_id=0&program_id=0&scope=root', t: '开发文档' },
    { h: 'https://e.gitee.com/cheerz/doc/share/c0dd8f70acdda0af/?sub_id=783706', t: '护理APP文档' },
    { h: 'http://0.0.0.0:8083/?openid=99999-0000000018&targetRoute=/#/', t: '移动资产前端调试地址' },
    { h: 'http://localhost:8082/modules/aio.html#/login', t: '一体机调试地址' },
    { h: 'http://localhost:8082/modules/index.html#/login', t: '高值柜调试地址' },
    { h: 'http://localhost:8082/modules/admin.html#/login', t: '军队医疗机构医疗设备配备标准核对系统' },
  ];

  constructor(private store: Store) {

    this.store.select<UserCache>((state: any) => {
      return state.usercache3
    }).pipe().subscribe(el => {
      this.userCache = el
    })


  }

  ngOnInit(): void {

  }

}
