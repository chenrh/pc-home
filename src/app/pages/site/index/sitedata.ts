

interface SiteModel {
    name: string
    site: string
}

interface MapSiteModel {
    keyName: string,
    category: string,
    listSite: SiteModel[]
}

const siteList: MapSiteModel[] = [
    {
        keyName: "fe",
        category: "大前端",
        listSite: [
            { name: 'Node.js', site: 'https://nodejs.org/zh-cn/' },
            { name: 'Vue.js', site: 'https://cn.vuejs.org/' },
            { name: 'Angular', site: 'https://angular.io/' },
            { name: '微信官方文档', site: 'https://developers.weixin.qq.com/doc/' },
            { name: 'element ui', site: 'https://element.eleme.cn/#/zh-CN' },
            { name: 'vant v1', site: 'https://vant-contrib.gitee.io/vant/v1/#/zh-CN/intro' },
            { name: 'ng-zorro', site: 'http://ng.ant.design/' }
        ]
    },
    {
        keyName: "be",
        category: "大后端",
        listSite: [
            { name: 'asp.net core', site: 'https://dotnet.microsoft.com/en-us/' },
            { name: 'golang', site: 'https://golang.google.cn/' },
            { name: 'python', site: 'https://www.python.org/' },
            { name: 'nodejs', site: 'https://nodejs.org/zh-cn/' },
            { name: 'mysql', site: 'https://www.mysql.com/' }
        ]
    },
    {
        keyName: "tools",
        category: "工具",
        listSite: [
            { name: 'Visual Studio Code', site: 'https://code.visualstudio.com/' },
            { name: 'Visual Studio', site: 'https://visualstudio.microsoft.com/zh-hans/downloads/' },
            { name: 'git', site: 'https://git-scm.com/' },
            { name: 'nginx', site: 'http://nginx.org/' },
            { name: '谷歌翻译', site: 'https://translate.google.cn/' }
        ]
    },
    {
        keyName: "open",
        category: "开源",
        listSite: [
            { name: '微信SDK C#', site: 'https://gitee.com/fudiwei/DotNetCore.SKIT.FlurlHttpClient.Wechat' },
        ]
    },
    {
        keyName: "other",
        category: "其他",
        listSite: [
            { name: '谷歌翻译', site: 'https://translate.google.cn/' },
            { name: 'Gitee 文档', site: 'https://e.gitee.com/cheerz/docs?directory=996&page=1&program_id=0&scope=root' },
        ]
    }
]

export { SiteModel, MapSiteModel, siteList }