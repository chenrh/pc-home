import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteIndexComponent } from './index.component';

describe('IndexComponent', () => {
  let component: SiteIndexComponent;
  let fixture: ComponentFixture<SiteIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SiteIndexComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
