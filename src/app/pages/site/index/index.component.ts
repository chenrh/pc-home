import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { SiteModel, MapSiteModel, siteList } from './sitedata'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less']
})
export class SiteIndexComponent implements OnInit {

  dataSet: MapSiteModel
  constructor(private route: ActivatedRoute, private router: Router) {
    this.dataSet = { keyName: '', category: '', listSite: [] };
  }

  ngOnInit(): void {
    this.loadPage();
    this.router.events.subscribe((value) => {
      if (value instanceof NavigationEnd) {
        this.loadPage();
      }
    });
  }

  loadPage(): void {
    const id = this.route.snapshot.paramMap.get('id');
    let site = siteList.find(el => el.keyName === id);
    if (site) {
      this.dataSet = site;
    }
  }

}
