import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserCache } from '@/state/UserCache';
import { saveUserCacheAction } from '@/state/user.actions'
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UserService } from '@/service/user.service'
import { GiteeUser } from '@/models/GiteeUser';
import { Router } from '@angular/router';
import { Menu } from '@/models/Menu';
import { MenuService } from '@/service/menu.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.less']
})
export class LayoutComponent implements OnInit {
  userCache?: UserCache = void 0
  // user$ = this.store.select(selectUserCache)
  isCollapsed = true;
  isLayoutModalVisible = false

  settingKind = ''
  namespace = ''
  dbconnectionstr = ''

  user?: GiteeUser = void 0
  menuData: Array<Menu> = [];

  constructor(
    private store: Store<{ usercache: UserCache }>,
    private notification: NzNotificationService,
    private us: UserService,
    private menuSrv: MenuService,

    private router: Router) {
    this.namespace = localStorage.getItem('namespace') || ''
  }
  ngOnInit(): void {
    // this.user$.subscribe(el => {
    //   this.userCache = el;
    // });
    // let usercache: UserCache = { UserId: 'a', UserCode: 'b', UserName: 'c', Token: "1111", }
    // this.store.dispatch(saveUserCacheAction({ userCache: usercache }));

    this.us.getUser().subscribe(el => {
      this.user = el;
      let usercache: UserCache = { UserId: el.login, UserCode: el.login, UserName: el.name, Token: "1111", }
      this.store.dispatch(saveUserCacheAction({ userCache: usercache }));
    })

    this.menuSrv.getMenuTree().subscribe(el => {
      for (let i = 0; i < el.length; i++) {
        let menuitem = el[i];
        if (!menuitem.is_leaf) {
          menuitem.expand = true;
          break;
        }
      }
      this.menuData = el;
    })
  }


  setting(kind: string) {
    this.settingKind = kind
    if (kind === 'db') {
      this.isLayoutModalVisible = true;
    }
    else if (kind === 'namespace') {
      this.isLayoutModalVisible = true;
    }
    else if (kind === 'author') {
      this.notification.create(
        'success',
        '软件作者',
        `${this.user?.name} 邮箱:${this.user?.email}`
      );
    }
    else if (kind === 'logout') {
      this.us.logout().subscribe(() => {
        this.router.navigateByUrl("/login")
      })
    }
  }

  handleModalOk(kind: string) {
    if (kind === 'namespace') {
      localStorage.setItem('namespace', this.namespace)
    }
    this.isLayoutModalVisible = false;
  }

  handleModalCancel() {
    this.isLayoutModalVisible = false;
  }

  json2struct() {
    this.router.navigateByUrl("/layout/json2struct")
  }
  text2qrcode() {
    this.router.navigateByUrl("/layout/text2qrcode")
  }

  // 子组件提交的事件
  onSubmenuOpened(el: Menu) {
    for (let i = 0; i < this.menuData.length; i++) {
      let menuitem = this.menuData[i];
      if (!menuitem.is_leaf) {
        menuitem.expand = menuitem.menu_id == el.menu_id;
      }
    }
  }
  // ng g c pages/layout --module=app
}
