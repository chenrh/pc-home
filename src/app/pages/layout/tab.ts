interface NavTab {
    id: string
    title: string
    path: string // full router link
    link: string // router link
}

export { NavTab }