import { UserCache } from '@/state/UserCache';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { softList, MapSoftwareModel } from './pcdata'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less']
})
export class PCIndexComponent implements OnInit {
  dataSet: MapSoftwareModel
  userCache$: Observable<UserCache>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<{ usercache3: UserCache }>) {
    this.userCache$ = this.store.select('usercache3');
    this.dataSet = { keyName: '', category: '', listSoft: [] };
  }

  ngOnInit(): void {
    this.loadPage();

    this.router.events.subscribe((value) => {
      if (value instanceof NavigationEnd) {
        this.loadPage();
      }
    });

    this.userCache$.subscribe(el => {
      console.log(el)
    })
  }

  loadPage(): void {
    const id = this.route.snapshot.paramMap.get('id');
    let site = softList.find(el => el.keyName === id);
    if (site) {
      this.dataSet = site;
    }
  }

}
