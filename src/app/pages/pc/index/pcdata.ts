

interface SoftwareModel {
    name: string
    desc: string
    code: string
}

interface MapSoftwareModel {
    keyName: string,
    category: string,
    listSoft: SoftwareModel[]
}

const softList: MapSoftwareModel[] = [
    {
        keyName: "macpro",
        category: "Mac Pro",
        listSoft: [
            { name: 'node', desc: 'node 16.13.0', code: 'node --version' },
            { name: 'nginx', desc: 'nginx 1.20.5', code: 'code /usr/local/etc/nginx/nginx.conf' },
            { name: 'git', desc: 'git 2.33.0', code: 'code /usr/local/bin/git' },
        ]
    }, {
        keyName: "macmini",
        category: "Mac Mini",
        listSoft: [
            { name: 'node', desc: 'node 16.13.0', code: 'node --version' },
            { name: 'nginx', desc: 'nginx 1.20.5', code: 'code /usr/local/etc/nginx/nginx.conf' },
            { name: 'git', desc: 'git 2.30.1', code: 'code /usr/local/bin/git' },
        ]
    }, {
        keyName: "lenovo",
        category: "联想笔记本",
        listSoft: [
            { name: 'node', desc: 'node 12.22.0', code: 'node --version' },
            { name: 'nginx', desc: 'nginx 1.21.4', code: 'code C:\\intpub\\nginx-1.21.4\\conf\\nginx.conf' },
            { name: 'git', desc: 'git 2.33.0', code: 'git --version' },
            { name: 'openVPN GUI', desc: 'git 2.33.0', code: 'C:\\CODE2021\\thoth_server\\OpenVPN\bin' },
        ]
    },
]

export { SoftwareModel, MapSoftwareModel, softList }