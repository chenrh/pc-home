import { Menu } from '@/models/Menu';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-menuitem]',
  templateUrl: './menuitem.component.html',
  styleUrls: ['./menuitem.component.less']
})
export class MenuitemComponent {

  @Input() treeData: Array<Menu> = [];
  @Output() submenuOpened = new EventEmitter<Menu>();

  constructor() {
  }

  openHandler(clickedMenu: Menu) {
    this.submenuOpened.emit(clickedMenu);
  }
}
