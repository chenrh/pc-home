import { ApiPagination } from '@/models/ApiPagination';
import { ToolService } from '@/service/tool.service';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';

@Component({
  selector: 'app-text2qrcode',
  templateUrl: './text2qrcode.component.html',
  styleUrls: ['./text2qrcode.component.less']
})
export class Text2qrcodeComponent implements OnInit {

  qrcodeText?: string
  qrcodeSrc?: string

  dataSet: any = []
  page: ApiPagination = { page: 1, per_page: 10, total: 0 }

  checked = false;
  indeterminate = false;
  setOfCheckedId = new Set<number>();

  constructor(
    private message: NzMessageService,
    private tool: ToolService,
    private modal: NzModalService,
  ) { }

  ngOnInit(): void {
  }

  translate() {
    if (this.qrcodeText?.length == 0) {
      this.message.error("请输入要转化的文本");
      return;
    }
    this.qrcodeSrc = "/api/qrcode?txt=" + this.qrcodeText
  }

  loadData() {
    this.tool.getQRRecord(this.page).subscribe(el => {
      this.dataSet = el.data;
      this.page.total = el.total;
    })
  }
  onQueryParamsChange(params: NzTableQueryParams): void {
    // console.log(params)
    const { pageSize, pageIndex, sort, filter } = params;
    this.page.page = pageIndex;
    this.page.per_page = pageSize;

    // const currentSort = sort.find(item => item.value !== null);
    // const sortField = (currentSort && currentSort.key) || "full_name";
    // const sortOrder = (currentSort && currentSort.value) || "asc";

    // this.page.sort = sortField;
    // this.page.direction = sortOrder;

    this.loadData()
  }

  // 单选，多选，全选
  onAllChecked(checked: boolean): void {
    this.dataSet
      .forEach(({ code_id }: any) => this.updateCheckedSet(code_id, checked));
    this.refreshCheckedStatus();
  }
  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }
  refreshCheckedStatus(): void {
    const listOfEnabledData = this.dataSet;
    this.checked = listOfEnabledData.every(({ code_id }: any) => this.setOfCheckedId.has(code_id));
    this.indeterminate = listOfEnabledData.some(({ code_id }: any) => this.setOfCheckedId.has(code_id)) && !this.checked;
  }

  // 事件
  removeRows() {
    let hasChecked = this.setOfCheckedId.size > 0;
    if (!hasChecked) {
      this.message.error("请选择中要删除的记录");
      return;
    }

    this.modal.confirm({
      nzTitle: "您确认要删除吗",
      nzContent: "请注意，删除后无法复原！",
      nzOnOk: () => {
        let arr = Array.from(this.setOfCheckedId);
        this.tool.deleteQRRecord(arr).subscribe(el => {
          this.message.error(el.message);
          if (el.flag) {
            this.page.page = 1;
            this.loadData()
          }
        })
      }
    })

  }

}
