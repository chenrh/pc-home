import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Text2qrcodeComponent } from './text2qrcode.component';

describe('Text2qrcodeComponent', () => {
  let component: Text2qrcodeComponent;
  let fixture: ComponentFixture<Text2qrcodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Text2qrcodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Text2qrcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
