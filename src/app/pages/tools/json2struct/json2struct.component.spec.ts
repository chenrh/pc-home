import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Json2structComponent } from './json2struct.component';

describe('Json2structComponent', () => {
  let component: Json2structComponent;
  let fixture: ComponentFixture<Json2structComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Json2structComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Json2structComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
