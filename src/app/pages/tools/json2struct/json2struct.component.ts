import { ToolService } from '@/service/tool.service';
import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-json2struct',
  templateUrl: './json2struct.component.html',
  styleUrls: ['./json2struct.component.less']
})
export class Json2structComponent implements OnInit {

  jsonstr?: string
  structstr?: string
  constructor(private message: NzMessageService, private tool: ToolService) { }

  ngOnInit(): void {
  }

  translate() {
    console.log(this.jsonstr)
    if (this.jsonstr?.length == 0) {
      this.message.error("请输入要转化的源json数据");
      return;
    }

    console.log(this.jsonstr)

    this.tool.postJSON2Struct(this.jsonstr).subscribe(el => {
      this.structstr = el.message;
    })

  }
}
