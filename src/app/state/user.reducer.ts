import { createReducer, on } from '@ngrx/store';
import { saveUserCacheAction } from './user.actions';
import { UserCache } from '@/state/UserCache';

export const initialUserState: Readonly<UserCache> = { Token: '', UserCode: '', UserId: '', UserName: '' } as UserCache;

export const userCacheReducer = createReducer(
    initialUserState,
    on(saveUserCacheAction, (state, { userCache }) => userCache)
);
