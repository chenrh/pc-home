
export interface UserCache {
    Token: string
    UserCode: string
    UserId: string
    UserName: string
}