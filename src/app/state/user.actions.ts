// https://ngrx.io/guide/store/actions

import { createAction, props } from '@ngrx/store';
import { UserCache } from './UserCache';

export const saveUserCacheAction = createAction(
    '[UserCache API] add Login Success UserInfo',
    props<{ userCache: Readonly<UserCache> }>()
);