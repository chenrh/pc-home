export interface NodeModel {
    id: string
    title: string
    url?: string | null
    children?: Array<NodeModel> | null
}