interface GiteeRepo {
    owner?: string
    repo?: string
}

export { GiteeRepo }