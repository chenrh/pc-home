interface GiteeUser {
    avatar_url: string
    bio: string
    blog: any
    created_at: string
    email: string
    events_url: string
    followers: number
    followers_url: string
    following: number
    following_url: string
    gists_url: string
    html_url: string
    id: number
    login: string
    name: string
    organizations_url: string
    public_gists: number,
    public_repos: number,
    received_events_url: string
    remark: string
    repos_url: string
    stared: number,
    starred_url: string
    subscriptions_url: string
    type: string
    updated_at: string
    url: string
    watched: string,
    weibo: string
}

export { GiteeUser }