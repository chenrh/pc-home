// {
// "flag": false,
// "message": "用户不存在",
// "errors": null,
// "data": null
// }
interface ApiResponse {
    flag: boolean
    message: string
    errors: any
    data: any
}

interface ApiPageResponse {
    total: number
    data: Array<any>
}


export { ApiResponse, ApiPageResponse }