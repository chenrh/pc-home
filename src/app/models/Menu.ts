export interface Menu {
    menu_id: number
    menu_name: string
    parent_id: number
    menu_path?: string
    children?: Array<any> | null | undefined
    is_leaf: boolean,
    expand?: boolean,
    menu_icon?: string,
    order_num?: number,
}