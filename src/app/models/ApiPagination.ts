import { GiteeRepo } from "./GiteeRepo"

interface ApiPagination {
    total: number
    page: number
    per_page: number
    sort?: string
    direction?: string
    q?: string
}


interface ApiIssuesPagination extends ApiPagination, GiteeRepo {
    // Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
    state: "open" | "progressing" | "closed" | "rejected",
}

export { ApiPagination, ApiIssuesPagination }