import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiPageResponse, ApiResponse } from '@/models/ApiResponse';
import { Menu } from '@/models/Menu';
import { ApiPagination } from '@/models/ApiPagination';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) { }

  // 保存菜单
  createMenu(menu: Menu): Observable<ApiResponse> {
    let url = `api/menu/create`
    return this.http.post<ApiResponse>(url, menu, {});
  }

  // 获得菜单实体数据
  getMenu(menu_id: number): Observable<ApiResponse> {
    let url = `api/menu/get?menu_id=` + menu_id;
    return this.http.get<ApiResponse>(url, {});
  }

  // 获得菜单树结构数据
  getMenuTree(): Observable<Array<Menu>> {
    let url = `api/menu/tree`;
    return this.http.get<Array<Menu>>(url);
  }
}
