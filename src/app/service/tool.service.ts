import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiPagination, ApiIssuesPagination } from '@/models/ApiPagination';
import { ApiResponse } from '@/models/ApiResponse';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  constructor(private http: HttpClient) { }

  // 将 json string 转化为 golang struct
  postJSON2Struct(formdata: any): Observable<any> {
    let url = `api/json2struct`
    return this.http.post<any>(url, formdata, {});
  }

  getQRRecord(page: ApiPagination): Observable<any> {
    let url = `api/qrcode/list`
    return this.http.get<any>(url, {
      params: {
        ...page
      }
    });
  }

  deleteQRRecord(arr: Array<number>): Observable<ApiResponse> {
    let url = `api/qrcode/delete`;
    return this.http.post<ApiResponse>(url, arr);
  }
}
