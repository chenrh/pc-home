import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiPagination, ApiIssuesPagination } from '@/models/ApiPagination';

@Injectable({
  providedIn: 'root'
})
export class RepoService {

  constructor(private http: HttpClient) { }

  // 获取 git 仓库
  getRepos(page: ApiPagination): Observable<any> {
    let url = `api/repos`
    return this.http.get<any>(url, {
      params: {
        ...page
      }
    });
  }

  // 获取 git issues
  getIssues(page: ApiIssuesPagination): Observable<any> {
    let url = `api/issues`
    return this.http.get<any>(url, {
      params: {
        ...page
      }
    });
  }

  // 获取企业版本 git issues
  getEnterprisesIssues(page: ApiIssuesPagination): Observable<any> {
    let url = `api/issues/enterprises`
    return this.http.get<any>(url, {
      params: {
        ...page
      }
    });
  }

  createIssue(formdata: any): Observable<any> {
    let url = `api/issues/create`
    return this.http.post<any>(url, formdata, {});
  }
}
