import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiPageResponse, ApiResponse } from '@/models/ApiResponse';
import { Menu } from '@/models/Menu';
import { ApiPagination } from '@/models/ApiPagination';

@Injectable({
  providedIn: 'root'
})
export class DocService {

  constructor(private http: HttpClient) { }

  // 获取文档列表
  getDocs(page: ApiPagination): Observable<any> {
    let url = `api/docs`
    return this.http.get<any>(url, {
      params: {
        ...page
      }
    });
  }

  // 获取文档信息
  getDocByID(id: string): Observable<any> {
    let url = `api/doc/${id}`
    return this.http.get<any>(url, {
      params: {

      }
    });
  }

  getDocConctentByPath(doc_path: string): Observable<string> {
    let url = `${doc_path}`
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    const requestOptions: Object = {
      headers: headers,
      responseType: 'text'
    }
    return this.http.get<string>(url, requestOptions);
  }

  // 保存文档
  saveDoc(formdata: any): Observable<ApiResponse> {
    let url = `api/doc/create`
    return this.http.post<ApiResponse>(url, formdata, {});
  }

}
