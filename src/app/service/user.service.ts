import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '@/models/ApiResponse';
import { GiteeUser } from '@/models/GiteeUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUser(): Observable<GiteeUser> {
    let url = `api/user`
    return this.http.get<GiteeUser>(url, {});
  }

  // 获得钉钉扫码登录的URL地址
  getDingtalkURL(): Observable<any> {
    let url = `dd/url`
    return this.http.get<any>(url, {});
  }

  // 登出
  logout(): Observable<any> {
    let url = `dd/logout`
    return this.http.get<any>(url, {});
  }

  tryLogin(username: string, password: string): Observable<any> {
    // api/Login/TryLogin?username=ad&password=afe
    let url = `api/ulogin?username=${username}&password=${password}`
    return this.http.post<any>(url, null, {});
  }

  test(): Observable<ApiResponse> {
    // api/Login/TryLogin?username=ad&password=afe
    let url = `api`
    return this.http.get<ApiResponse>(url, {});
  }
}
