// import {
//     MenuFoldOutline,
//     MenuUnfoldOutline,
//     FormOutline,
//     DashboardOutline,
//     CheckOutline,
//     CodeOutline,
//     SettingOutline,
//     ProjectOutline,
//     AppleOutline,
//     WindowsOutline,
//     PlusOutline,
// } from '@ant-design/icons-angular/icons';

// const icons = [
//     MenuFoldOutline,
//     MenuUnfoldOutline,
//     DashboardOutline,
//     FormOutline,
//     CheckOutline,
//     CodeOutline,
//     SettingOutline,
//     ProjectOutline,
//     AppleOutline,
//     WindowsOutline,
//     PlusOutline,
// ];

// export default icons

// 2022-4-1 注册所有图标
import * as icons from '@ant-design/icons-angular/icons'
export default Object.values(icons);


// icons-provider.module.ts

// import { NgModule } from '@angular/core';
// import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

// import {
//   MenuFoldOutline,
//   MenuUnfoldOutline,
//   FormOutline,
//   DashboardOutline,
//   CheckOutline
// } from '@ant-design/icons-angular/icons';

// const icons = [
//   MenuFoldOutline,
//   MenuUnfoldOutline,
//   DashboardOutline,
//   FormOutline,
//   CheckOutline
// ];

// @NgModule({
//   imports: [NzIconModule],
//   exports: [NzIconModule],
//   providers: [
//     { provide: NZ_ICONS, useValue: icons }
//   ]
// })
// export class IconsProviderModule {
// }
